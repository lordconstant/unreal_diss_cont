// Fill out your copyright notice in the Description page of Project Settings.

#include "MP_Whitebox.h"
#include "LeverActor.h"

ALeverActor::ALeverActor(const class FObjectInitializer& PCIP)
	: Super(PCIP)
{
	PrimaryActorTick.bCanEverTick = true;

	arrowComp = PCIP.CreateDefaultSubobject<USceneComponent>(this, TEXT("ArrowComp"));
	staticMeshComp = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("StaticMeshComp"));
	triggerArea = PCIP.CreateDefaultSubobject<USphereComponent>(this, TEXT("TriggerArea"));

	RootComponent = arrowComp;
	staticMeshComp->AttachParent = RootComponent;
	triggerArea->AttachParent = RootComponent;

	m_usedLever = false;
	m_inputKeyPressed = false;
}

bool ALeverActor::interactWithLever(AActor* player){
	if (!m_usedLever){
		if (m_inputKeyPressed){
			UE_LOG(LogTemp, Log, TEXT("Key Pressed"));
			if (IsOverlappingActor(player)){
				m_usedLever = true;
				return true;
			}
		}
	}

	m_usedLever = true;
	return false;
}

void ALeverActor::inputKeyPressed(){
	if (m_inputKeyPressed){
		m_inputKeyPressed = false;
	}else{
		m_inputKeyPressed = true;
	}

	m_usedLever = false;
}

void ALeverActor::BeginPlay(){
	Super::BeginPlay();

	InputComponent = ConstructObject<UInputComponent>(UInputComponent::StaticClass(), this, "Lever Input Component");
	InputComponent->RegisterComponent();
	InputComponent->bBlockInput = bBlockInput;

	if (InputComponent){
		InputComponent->BindAction("UseLever", IE_Pressed, this, &ALeverActor::inputKeyPressed);
		InputComponent->BindAction("UseLever", IE_Released, this, &ALeverActor::inputKeyPressed);

		EnableInput(GetWorld()->GetFirstPlayerController());
	}
}