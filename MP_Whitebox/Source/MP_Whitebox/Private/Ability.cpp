// Fill out your copyright notice in the Description page of Project Settings.

#include "MP_Whitebox.h"
#include "Ability.h"


UAbility::UAbility(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	PrimaryComponentTick.bCanEverTick = true;

	bAutoActivate = true;
	bAutoRegister = true;
}

void UAbility::startAbility(AActor* player){
	return;
}

void UAbility::useAbility(AActor* player){
	return;
}

void UAbility::endAbility(AActor* player){
	return;
}

void UAbility::stopAbility(){
	return;
}

void UAbility::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction){
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	return;
}

bool UAbility::isChanneled(){
	return m_channeled;
}

void UAbility::isChanneled(bool channeling){
	m_channeled = channeling;
}