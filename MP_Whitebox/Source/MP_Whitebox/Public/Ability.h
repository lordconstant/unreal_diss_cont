// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "Ability.generated.h"

/**
 * 
 */
UCLASS()
class MP_WHITEBOX_API UAbility : public UActorComponent
{
	GENERATED_UCLASS_BODY()

	bool m_channeled;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AbilityProperties)
		float CastDistance;

	virtual void startAbility(AActor* player);
	virtual void useAbility(AActor* player);
	virtual void endAbility(AActor* player);
	virtual void stopAbility();

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

	bool isChanneled();
	void isChanneled(bool channeling);
};
