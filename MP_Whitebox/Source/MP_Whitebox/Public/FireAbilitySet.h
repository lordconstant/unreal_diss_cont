// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AbilitySet.h"
#include "FireballAbility.h"
#include "FireAbilitySet.generated.h"

/**
 * 
 */

enum FIREPOWERS{FIREBALL = 0};

UCLASS()
class MP_WHITEBOX_API UFireAbilitySet : public UAbilitySet
{
	GENERATED_UCLASS_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Abilities)
	UFireballAbility* FireballAbility;

	void initialisePowers(const class FObjectInitializer& PCIP, AActor* parent);
	void init();

	void startAbility(uint32 ability, AActor* player);
	void useAbility(uint32 ability, AActor* player);
	void endAbility(uint32 ability, AActor* player);
	void stopAbility(uint32 ability);

	bool isChanneled(uint32 ability);
	void isChanneled(uint32 ability, bool channeling);
	
	
};
