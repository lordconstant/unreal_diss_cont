// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Ability.h"
#include "WindPushAbility.generated.h"

/**
 * 
 */

UCLASS()
class MP_WHITEBOX_API UWindPushAbility : public UAbility
{
	GENERATED_UCLASS_BODY()

	AActor* m_curMarker;
	AActor* m_windParticles;

	bool m_showingMarker;
	bool m_windBlowing;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AbilityProperties)
	TSubclassOf<AActor> WindMarker;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AbilityProperties)
	TSubclassOf<AActor> WindParticles;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AbilityProperties)
	float PushForce;

	void startAbility(AActor* player);
	void useAbility(AActor* player);
	void endAbility(AActor* player);
	void stopAbility();

	UFUNCTION(BlueprintCallable, Category = Ability)
	bool isWindBlowing();

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
};
